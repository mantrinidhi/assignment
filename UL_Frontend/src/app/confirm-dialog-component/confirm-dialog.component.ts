import { Component, OnInit } from '@angular/core';
import { ConfirmDialogService } from '../services/confirm-dialog.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

  message: any;
  constructor(
    private confirmDialogService: ConfirmDialogService
  ) { }

  ngOnInit(): any {
    this.confirmDialogService.subject.subscribe(message => {
      this.message = message;
    });
  }
}
