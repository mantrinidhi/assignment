import { Component, OnInit } from '@angular/core';
import { HttpClientService } from '../services/http-client.service';
import { ConfirmDialogService } from '../services/confirm-dialog.service';
import { Router } from '@angular/router';
import { IProject } from '../model/IProject';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  public projectList: Array<IProject> = [];

  constructor(private httpClient: HttpClientService, private confirmDialogService: ConfirmDialogService, private router: Router) {
  }

  ngOnInit() {
    this.getAllProject();
  }

  public addProject() {
    this.router.navigate(['/add-project']);
  }

  // this function is used for edit project detail
  public editProject(id) {
    const project:IProject = this.projectList.find(project => project.id === id);
    this.confirmDialogService.confirmThis(`Are you sure that you want to archive (${project?.name})?`, () => {
      const apiURL: string = `projects/${id}`;
      this.httpClient.put(apiURL, {}).subscribe(data => {
        if (data) {
          this.getAllProject();
        }
      });
    }, () => {

    });
  }

  // this function to use to get all project list

  public getAllProject() {
    this.httpClient.get("projects").subscribe(data => {
      if (data) {
        this.projectList = data;
      }
    });
  }
}
