import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AddProjectComponent } from './add-project/add-project.component';
import { AddRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfirmDialogComponent } from './confirm-dialog-component/confirm-dialog.component';
import { HttpClientService } from './services/http-client.service';
import { ProjectListComponent } from './project-list/project-list.component';
import { ConfirmDialogService } from './services/confirm-dialog.service';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    ConfirmDialogComponent,
    AddProjectComponent,
    ProjectListComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AddRoutingModule
  ],
  exports:[ConfirmDialogComponent],
  providers: [HttpClientService, ConfirmDialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
