export interface IProject
{
  id: number,
  name: string,
  status: string,
  localDateTime: string,
  archived: string
}