import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  private apiUrl:string='';
  constructor(private httpClient: HttpClient) {
    this.apiUrl= `${environment["protocol"]}://${environment["url"]}${environment["endpoint"]}/`;
   }

  post(action, params): Observable<any> {
    const url: string = `${this.apiUrl}${action}`;
    return this.httpClient.post(url, params).pipe(
      map((res) => res));
  }

  get(action, params = {}): Observable<any> {
    const url: string = `${this.apiUrl}${action}`;
    return this.httpClient.get(url, { params: params }).pipe(
      map((res) => res));
  }

  put(action, params): Observable<any> {
    const url: string = `${this.apiUrl}${action}`;
    return this.httpClient.put(url, params).pipe(
      map((res) => res));
  }

  
}
