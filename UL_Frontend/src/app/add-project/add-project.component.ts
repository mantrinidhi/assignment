import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClientService } from '../services/http-client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {

  public addForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private httpService: HttpClientService, private router: Router) { }

  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      name: ['', Validators.required],
      status: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.addForm.invalid) {
      this.addForm.markAllAsTouched();
    }
    else {
      this.httpService.post('projects', this.addForm.value).subscribe(data => {
        console.log(data);
        this.router.navigate([''])
      });
    }
  }

  onReset() {
    this.addForm.reset();
  }
}
