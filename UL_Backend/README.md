# Assignment-backend-api

Read assignment instructions provided in pdf file.

Steps to  run project

1) Import project as maven in any editor
2) Run project as maven build by putting clean install command in goal
3) Once build success  start application
4) URL of get
   http://localhost:8080/projects
   Desc :- you will get all projects that are not archived
   
5) URL of post
   http://localhost:8080/projects
   Desc:-Insert new project
   Body:-{
			"name": "test",
			"status":"IN_PROGRESS"
		}
6) URL of Put
   http://localhost:8080/projects/1 
   

