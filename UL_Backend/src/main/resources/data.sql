
DROP TABLE IF EXISTS project;

CREATE TABLE project (
  id Long AUTO_INCREMENT  PRIMARY KEY,
  name varchar(250) NOT NULL,
  statusEnum ENUM('IN_PROGRESS', 'FINISHED'),
  localDateTime dateTime,
  archived boolean
);

INSERT INTO project (name, statusEnum, archived) VALUES
  ('Aliko', 'IN_PROGRESS', '1'),
  ('Bill', 'FINISHED', '0'),
  ('Folrunsho', 'IN_PROGRESS', '1'),
  ('UL', 'FINISHED', '0'),
  ('Folrunsho', 'FINISHED', '1');