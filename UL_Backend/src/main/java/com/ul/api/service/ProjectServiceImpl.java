package com.ul.api.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.ul.api.dto.ProjectRequest;
import com.ul.api.dto.ProjectResponse;
import com.ul.api.entity.Project;
import com.ul.api.repository.ProjectRepository;

@Service
public class ProjectServiceImpl implements ProjectService {

	private final ProjectRepository projectRepository;

	public ProjectServiceImpl(ProjectRepository projectRepository) {

		this.projectRepository = projectRepository;

	}

	@Override
	public ProjectResponse insertProject(ProjectRequest projectRequest) {
		Project project = new Project();
		project.setName(projectRequest.getName());
		project.setStatusEnum(projectRequest.getStatuEnum());
		project.setLocalDateTime(LocalDateTime.now());
		project.setArchived(false);
		project = projectRepository.save(project);

		ProjectResponse projectResponse = new ProjectResponse();
		projectResponse.setId(project.getId());
		projectResponse.setName(project.getName());
		projectResponse.setStatusEnum(project.getStatusEnum());
		projectResponse.setLocalDateTime(project.getLocalDateTime());
		projectResponse.setArchived(project.isArchived());

		return projectResponse;

	}

	@Override
	public List<ProjectResponse> getProjects() {

		List<Project> projects = projectRepository.findByNonArchivedProjects();
		List<ProjectResponse> projectResponses = projects.stream().map(project -> {
			ProjectResponse projectResponse = new ProjectResponse();
			projectResponse.setId(project.getId());
			projectResponse.setName(project.getName());
			projectResponse.setStatusEnum(project.getStatusEnum());
			projectResponse.setLocalDateTime(project.getLocalDateTime());
			projectResponse.setArchived(project.isArchived());

			return projectResponse;

		}).collect(Collectors.toList());

		return projectResponses;

	}

	@Override
	public ProjectResponse getProjectById(long id) {

		Optional<Project> opProject = projectRepository.findById(id);
		ProjectResponse projectResponse = new ProjectResponse();
		if (opProject.isPresent()) {

			Project project = opProject.get();
			project.setArchived(true);
			project.setLocalDateTime(LocalDateTime.now());
			project = this.projectRepository.save(project);
			projectResponse.setId(project.getId());
			projectResponse.setName(project.getName());
			projectResponse.setStatusEnum(project.getStatusEnum());
			projectResponse.setLocalDateTime(project.getLocalDateTime());
			projectResponse.setArchived(project.isArchived());
		}
		return projectResponse;

	}

}