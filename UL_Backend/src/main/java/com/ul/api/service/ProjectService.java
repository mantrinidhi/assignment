package com.ul.api.service;

import java.util.List;

import com.ul.api.dto.ProjectRequest;
import com.ul.api.dto.ProjectResponse;

public interface ProjectService {
	ProjectResponse insertProject(ProjectRequest projectRequest);
	List<ProjectResponse> getProjects();
	ProjectResponse getProjectById(long Id);
	
}




