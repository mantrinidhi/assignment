
package com.ul.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "message",
    "description",
    "type"
})
@Data
@NoArgsConstructor
public class Status 
{

    @JsonProperty("code")
    private String code;
    @JsonProperty("message")
    private String message;
    
    @JsonProperty("description")
    private String description;

    // Suggested values are Informational, Warning, Error
    @JsonProperty("type")
    private String type;
    
    
}
