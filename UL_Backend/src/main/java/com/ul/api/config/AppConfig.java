package com.ul.api.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.CollectionUtils;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;

import com.ul.api.dto.ApiErrorCode;
import com.ul.api.dto.ErrorCode;
import com.ul.api.model.Status;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Configuration
public class AppConfig {
	@Bean
	@Qualifier("errorCodeMap")
	public Map<String, Status> loadErrorMappingFile() {
		ErrorCode errorCode = null;
		Map<String, Status> errorCodeMap = new HashMap<>();
		log.info("Started loading error mapping file.");
		Yaml yaml = new Yaml();
		try (InputStream in = AppConfig.class.getResourceAsStream("/error-mapping.yaml")) {
			errorCode = yaml.loadAs(in, ErrorCode.class);

			if (!CollectionUtils.isEmpty(errorCode.getApiCodes())) {
				for (ApiErrorCode apiErrorCode : errorCode.getApiCodes()) {
					for (String sourceErrorCode : apiErrorCode.getSourceErrorCode()) {
						errorCodeMap.put(sourceErrorCode, apiErrorCode.getStatus());
					}
				}
			}

		} catch (IOException e) {
			log.error(
					"error-mapping.yaml file not found in resource folder. Exitting the application. Exception is : {}",
					e.getMessage());
			System.exit(-1);
		} catch (YAMLException e) {
			log.error(
					"Error occured while parsing the error-mapping.yaml file. Exitting the application. Exception is : {}",
					e.getMessage());
			System.exit(-1);
		}
		log.info("Successfully loaded error mapping file.");
		return errorCodeMap;
	}

}
