package com.ul.api.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ul.api.dto.ProjectStatus;

import lombok.Data;

@Data
@Entity
@Table(name = "project")
public class Project {
	
	
	// mark id as primary key
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "statusEnum")
	@Enumerated(EnumType.STRING)
	private ProjectStatus statusEnum;

	@Column(name = "localDateTime")
	private LocalDateTime localDateTime;

	@Column(name = "archived")
	private boolean archived;
	
	public Project() {}
	
	public Project(long id, String name, ProjectStatus statusEnum, LocalDateTime localDateTime,boolean archived) {
		super();
		this.id = id;
		this.name = name;
		this.statusEnum = statusEnum;
		this.localDateTime = localDateTime;
		this.archived=archived;
	}

}