package com.ul.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ul.api.dto.ProjectRequest;
import com.ul.api.dto.ProjectResponse;
import com.ul.api.service.ProjectService;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@RequestMapping(path = "/projects")
@CrossOrigin(origins = "*")
public class ProjectController {

	private final ProjectService projectService;
    
	@Autowired
	public ProjectController(ProjectService projectService) {
		this.projectService = projectService;
	}


	@GetMapping( produces = { MediaType.APPLICATION_JSON_VALUE })
	
	public List<ProjectResponse> getProjects() {
		log.info("Get all projects which are not archived ");
		return projectService.getProjects();

	}
	
	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public ProjectResponse insertProject(@RequestBody ProjectRequest projectRequest) {
		 log.info("New Project create ", projectRequest);
		return projectService.insertProject(projectRequest);

	}
	
	
	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ProjectResponse updateProject(@PathVariable String id) {
		log.info("Get project by Id", id);
		return projectService.getProjectById(Long.valueOf(id));

	}
	
	

	
	 
}
