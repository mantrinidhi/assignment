package com.ul.api.dto;

public enum ProjectStatus {
	IN_PROGRESS("IN_PROGRESS"),
	 
	FINISHED("FINISHED");
 
   
 
    private String depCode;
 
    private ProjectStatus(String depCode) {
 
        this.depCode = depCode;
    }
 
    public String getDepCode() {
 
        return this.depCode;
    }
}
