package com.ul.api.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ProjectResponse {

	@JsonProperty("id")
	private long id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("status")
	private ProjectStatus statusEnum;
	@JsonProperty("localDateTime")
	private LocalDateTime localDateTime;
	@JsonProperty("archived")
	private boolean archived;

}
