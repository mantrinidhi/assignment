package com.ul.api.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ul.api.model.Status;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiErrorCode {
  @JsonProperty("status")
  public Status status;
  @JsonProperty("sourceErrorCode")
  public List<String> sourceErrorCode;
}
