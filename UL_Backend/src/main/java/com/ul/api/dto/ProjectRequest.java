package com.ul.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.ul.api.validator.Enum;

import lombok.Data;

@Data
public class ProjectRequest {

	@JsonProperty("name")
	private String name;

	@JsonProperty("status")
	private ProjectStatus statuEnum;

}
