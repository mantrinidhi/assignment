package com.ul.api.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class EnumValidator implements ConstraintValidator<Enum, String>{
	
	private Enum annotation;
	
	@Override
	public void initialize(Enum annotation) {
		this.annotation = annotation;
	}
	
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean result = false;
		
		Object[] enumValues = this.annotation.enumClass().getEnumConstants();
		
		if(enumValues != null) {
			for (Object enumValue: enumValues) {
				if(value.equals(enumValue.toString())) {
					result = true;
					break;
				}
			}
		}
		
		if(!result) {
			log.warn("Project Status is invalid  {} is invalid.", value);
		}
		
		return result;
		
	}

}
