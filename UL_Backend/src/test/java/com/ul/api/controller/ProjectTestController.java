package com.ul.api.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ul.api.dto.ProjectRequest;
import com.ul.api.dto.ProjectResponse;
import com.ul.api.dto.ProjectStatus;
import com.ul.api.entity.Project;
import com.ul.api.repository.ProjectRepository;
import com.ul.api.service.ProjectServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class ProjectTestController {

	@InjectMocks
	ProjectServiceImpl projectService;

	@MockBean
	private ProjectRepository projectRepository;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		
	}

	/*
	 * @Test public void getProjectsTest() throws Exception {
	 * 
	 * when(projectRepository.findByNonArchivedProjects()).thenReturn(Stream. of(new
	 * Project(1,"test",ProjectStatus.FINISHED,LocalDateTime.now(),false), new
	 * Project(2,"test2",ProjectStatus.IN_PROGRESS,LocalDateTime.now(),false)).
	 * collect(Collectors.toList()));
	 * assertEquals(2,projectService.getProjects().size());
	 * 
	 * }
	 */

	@Test
	public void getAllProjects() {
		List<Project> toDoList = new ArrayList<Project>();
		toDoList.add(new Project(1, "test", ProjectStatus.FINISHED, LocalDateTime.now(), false));
		toDoList.add(new Project(2, "test2", ProjectStatus.IN_PROGRESS, LocalDateTime.now(), false));
		when(projectRepository.findByNonArchivedProjects()).thenReturn(toDoList);
		
		List<ProjectResponse> result = projectService.getProjects();
		assertEquals(2, result.size());
	}

	
	/*
	 * @Test public void testGetProjectById() { Project project = new Project(1,
	 * "test", ProjectStatus.FINISHED, LocalDateTime.now(), false);
	 * when(projectRepository.findById(1L)).thenReturn(Optional.of(project));
	 * ProjectResponse result = projectService.getProjectById(1); assertEquals(1,
	 * result.getId()); assertEquals("test", result.getName()); }
	 * 
	 * 
	 * 
	 * @Test public void saveProject() { Project project = new Project(9, "test",
	 * ProjectStatus.FINISHED, LocalDateTime.now(), false); ProjectRequest
	 * projectRequest= new ProjectRequest(); projectRequest.setName("test");
	 * projectRequest.setStatuEnum(ProjectStatus.FINISHED);
	 * when(projectRepository.save(project)).thenReturn(project); ProjectResponse
	 * result = projectService.insertProject(projectRequest); assertEquals(9,
	 * result.getId()); assertEquals("test", result.getName());
	 * 
	 * }
	 */
	 

}
